# Spring Test

Servidor dedicado al manejo de gateways que controlan múltiples Peripheral Devices.

## Server

### Requisitos para el sistema

- Java 17
- Maven
- Taskfile

### Pasos para correr el servidor

1. En primer lugar, se deben de configurar las variables de ambiente en un archivo .env, estas son las siguientes:
  - DATABASE_HOST
  - DATABASE_PORT
  - DATABASE_NAME
  - DATABASE_USER
  - DATABASE_PASSWORD

2. En segundo lugar, se ejecuta el siguiente comando: `task run` el cual internamente se encargara de importar las variables de ambiente del archivo .env y usara maven para la ejecución del servidor en modo de desarrollo.

## Documentación y Casos de Test

### Requisitos

- Insomnia

### Pasos para importar los datos

1. Abrir Insomnia
2. Crear una nueva colección de llamadas
3. Ya dentro de la nueva colección hacer click en el nombre de la colección en la esquina superior izquierda y entrar al menú de importar/exportar
4. En el menú darle click a Importar Data y seguidamente a Importar desde un archivo
5. Cargar el archivo `InsomniaTestData.json`
