package com.spring.example.schemas;

import com.spring.example.models.Gateway;
import lombok.Data;

import java.util.regex.Pattern;

@Data
public class GatewayDTO {
    private String serialNumber;
    private String name;
    private String address;

    public Gateway toGateway() {
        return new Gateway(serialNumber, name, address);
    }

    public Gateway updateGateway(Gateway gateway) {
        gateway.setSerialNumber(serialNumber);
        gateway.setName(name);
        gateway.setAddress(address);
        return gateway;
    }

    public Boolean isValid() {
        if (address == null || address.isEmpty()) {
            return false;
        }
        return pattern.matcher(address).matches();
    }

    private static final String REGEX = "(([0-1]?[0-9]{1,2}\\.)|(2[0-4][0-9]\\.)|(25[0-5]\\.)){3}(([0-1]?[0-9]{1,2})|(2[0-4][0-9])|(25[0-5]))";
    private static Pattern pattern = Pattern.compile(REGEX);
}
