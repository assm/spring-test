package com.spring.example.schemas;

import lombok.Data;

@Data
public class IdDTO<T> {
    private T id;
}
