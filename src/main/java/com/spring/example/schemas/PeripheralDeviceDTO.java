package com.spring.example.schemas;

import com.spring.example.models.Gateway;
import com.spring.example.models.PeripheralDevice;
import com.spring.example.models.enums.Status;
import com.spring.example.models.repositories.GatewayRepository;
import com.spring.example.models.repositories.PeripheralDeviceRepository;
import lombok.Data;

import java.util.Date;

@Data
public class PeripheralDeviceDTO {
    private Long uid;
    private String vendor;
    private Date created;
    private Status status;
    private String gateway;

    public PeripheralDevice toPeripheralDevice(GatewayRepository gatewayRepository, PeripheralDeviceRepository peripheralDeviceRepository) {
        var device = new PeripheralDevice(vendor, created, status);
        if(gateway != null) {
            var gatewayEntity = getGatewayFromId(gatewayRepository, peripheralDeviceRepository);
            if(gatewayEntity != null) {
                device.setGateway(gatewayEntity);
            }
        }
        return device;
    }

    public PeripheralDevice updateDevice(GatewayRepository gatewayRepository, PeripheralDeviceRepository peripheralDeviceRepository, PeripheralDevice device) {
        if(created == null) {
            created = new Date();
        }
        device.setVendor(vendor);
        device.setCreated(created);
        device.setStatus(status);
        if(gateway != null) {
            var gatewayEntity = getGatewayFromId(gatewayRepository, peripheralDeviceRepository);
            if(gatewayEntity != null) {
                device.setGateway(gatewayEntity);
            }
        }
        return device;
    }

    public void fromPeripheralDevice(PeripheralDevice device) {
        this.uid = device.getUid();
        this.vendor = device.getVendor();
        this.created = device.getCreated();
        this.status = device.getStatus();
        if(device.getGateway() != null) {
            this.gateway = device.getGateway().getId();
        }
    }

    private Gateway getGatewayFromId(GatewayRepository gatewayRepository, PeripheralDeviceRepository peripheralDeviceRepository) {
        var gatewayEntity = gatewayRepository.findById(gateway);
        if(gatewayEntity.isPresent()) {
            var devices = peripheralDeviceRepository.countAllByGateway(gatewayEntity.get());
            if(devices < 10) {
                return gatewayEntity.get();
            } else {
                return null;
            }
        } else {
            return null;
        }
    }
}
