package com.spring.example.models.repositories;

import com.spring.example.models.Gateway;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GatewayRepository extends JpaRepository<Gateway, String> {
    void deleteAllById(String id);
}
