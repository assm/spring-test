package com.spring.example.models.repositories;

import com.spring.example.models.Gateway;
import com.spring.example.models.PeripheralDevice;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PeripheralDeviceRepository extends JpaRepository<PeripheralDevice, Long> {
    Integer countAllByGateway(Gateway gateway);
    void deleteAllByUid(Long uid);
}
