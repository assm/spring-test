package com.spring.example.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.spring.example.models.enums.Status;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
public class PeripheralDevice {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long uid;
    private String vendor;
    private Date created;
    private Status status;

    @ManyToOne(targetEntity = Gateway.class, fetch = FetchType.LAZY, cascade=CascadeType.MERGE)
    @JsonBackReference
    private Gateway gateway;

    public PeripheralDevice() {}

    public PeripheralDevice(String vendor, Date created, Status status) {
        this.vendor = vendor;
        this.created = created;
        this.status = status;
    }
}
