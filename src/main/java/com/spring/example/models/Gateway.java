package com.spring.example.models;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
public class Gateway {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    private String id;
    private String serialNumber;
    private String name;
    private String address;

    @OneToMany(targetEntity = PeripheralDevice.class, fetch = FetchType.LAZY, mappedBy = "gateway", cascade=CascadeType.MERGE)
    @Cascade({org.hibernate.annotations.CascadeType.DELETE})
    @JsonManagedReference
    private List<PeripheralDevice> devices;

    public Gateway() {}

    public Gateway(String serialNumber, String name, String address) {
        this.serialNumber = serialNumber;
        this.name = name;
        this.address = address;
    }
}
