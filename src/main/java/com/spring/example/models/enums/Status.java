package com.spring.example.models.enums;

public enum Status {
    ONLINE,
    OFFLINE
}
