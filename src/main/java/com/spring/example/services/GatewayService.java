package com.spring.example.services;

import com.spring.example.models.Gateway;
import com.spring.example.models.repositories.GatewayRepository;
import com.spring.example.models.repositories.PeripheralDeviceRepository;
import com.spring.example.schemas.GatewayDTO;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public record GatewayService(PeripheralDeviceRepository peripheralDeviceRepository, GatewayRepository gatewayRepository) {

    public List<Gateway> getAll() {
        return this.gatewayRepository.findAll();
    }

    public Gateway getOne(String id) {
        var gateway = this.gatewayRepository.findById(id);
        if (gateway.isEmpty()) {
            return null;
        }
        return gateway.get();
    }

    public Gateway create(GatewayDTO request) {
        if (Boolean.FALSE.equals(request.isValid())) {
            return null;
        }
        var gateway = request.toGateway();
        this.gatewayRepository.save(gateway);
        return gateway;
    }

    public Gateway update(String id, GatewayDTO request) {
        if (Boolean.FALSE.equals(request.isValid())) {
            return null;
        }

        var gateway = this.gatewayRepository.findById(id);
        if (gateway.isEmpty()) {
            return null;
        }

        var gatewayEntity = request.updateGateway(gateway.get());
        this.gatewayRepository.save(gatewayEntity);
        return gatewayEntity;
    }

    public void delete(String id) {
        this.gatewayRepository.deleteAllById(id);
    }

    public Gateway addDevice(String gatewayId, Long deviceId) {
        var device = this.peripheralDeviceRepository.findById(deviceId);
        if(device.isEmpty()) {
            return null;
        }
        var gateway = this.gatewayRepository.findById(gatewayId);
        if(gateway.isEmpty()) {
            return null;
        }

        var gatewayEntity = gateway.get();
        var deviceEntity = device.get();

        if(gatewayEntity.getDevices().size() >= 10) {
            return null;
        }

        deviceEntity.setGateway(gateway.get());
        this.peripheralDeviceRepository.save(deviceEntity);

        var devices = gatewayEntity.getDevices();
        devices.add(deviceEntity);
        gatewayEntity.setDevices(devices);

        return gatewayEntity;
    }

    public Gateway removeDevice(String gatewayId, Long deviceId) {
        var gateway = this.gatewayRepository.findById(gatewayId);
        if(gateway.isEmpty()) {
            return null;
        }

        var device = this.peripheralDeviceRepository.findById(deviceId);
        if(device.isEmpty()) {
            return null;
        }

        var gatewayEntity = gateway.get();
        var deviceEntity = device.get();
        var devices = gatewayEntity.getDevices();
        var isValid = false;
        var index = -1;

        for(var i = 0; i < devices.size(); i++) {
            if (Objects.equals(devices.get(i).getUid(), deviceId)) {
                isValid = true;
                index = i;
                break;
            }
        }

        if(!isValid) {
            return null;
        }

        deviceEntity.setGateway(null);
        this.peripheralDeviceRepository.save(deviceEntity);

        devices.remove(index);

        gatewayEntity.setDevices(devices);

        return gatewayEntity;
    }
}
