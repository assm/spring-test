package com.spring.example.services;

import com.spring.example.models.PeripheralDevice;
import com.spring.example.models.repositories.GatewayRepository;
import com.spring.example.models.repositories.PeripheralDeviceRepository;
import com.spring.example.schemas.PeripheralDeviceDTO;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public record PeripheralDeviceService(PeripheralDeviceRepository peripheralDeviceRepository, GatewayRepository gatewayRepository) {

    public List<PeripheralDeviceDTO> getAll() {
        List<PeripheralDeviceDTO> elements = new ArrayList<>();
        for(var device : this.peripheralDeviceRepository.findAll()) {
            var deviceElement = new PeripheralDeviceDTO();
            deviceElement.fromPeripheralDevice(device);
            elements.add(deviceElement);
        }
        return elements;
    }

    public PeripheralDeviceDTO getOne(Long id) {
        var device = this.peripheralDeviceRepository.findById(id);
        if (device.isEmpty()) {
            return null;
        }

        var deviceElement = new PeripheralDeviceDTO();
        deviceElement.fromPeripheralDevice(device.get());
        return deviceElement;
    }

    public PeripheralDevice create(PeripheralDeviceDTO request) {
        var device = request.toPeripheralDevice(gatewayRepository, peripheralDeviceRepository);
        if (device == null) {
            return null;
        }

        this.peripheralDeviceRepository.save(device);
        return device;
    }

    public PeripheralDevice update(Long id, PeripheralDeviceDTO request) {
        var device = this.peripheralDeviceRepository.findById(id);
        if (device.isEmpty()) {
            return null;
        }

        var deviceEntity = request.updateDevice(gatewayRepository, peripheralDeviceRepository, device.get());
        if (deviceEntity == null) {
            return null;
        }

        this.peripheralDeviceRepository.save(deviceEntity);
        return deviceEntity;
    }

    public void delete(Long id) {
        this.peripheralDeviceRepository.deleteAllByUid(id);
    }
}
