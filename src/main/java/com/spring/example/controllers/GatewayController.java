package com.spring.example.controllers;

import com.spring.example.models.Gateway;
import com.spring.example.models.repositories.GatewayRepository;
import com.spring.example.schemas.GatewayDTO;
import com.spring.example.schemas.IdDTO;
import com.spring.example.services.GatewayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/gateway")
public class GatewayController {
    private final GatewayService gatewayService;

    @Autowired
    public GatewayController(GatewayService gatewayService) {
        this.gatewayService = gatewayService;
    }

    @GetMapping("")
    public List<Gateway> getAll() {
        return this.gatewayService.getAll();
    }

    @GetMapping("/{id}")
    public Gateway getOne(@PathVariable("id") String id) {
        return this.gatewayService.getOne(id);
    }

    @PostMapping("")
    public Gateway create(@RequestBody GatewayDTO request) {
        return this.gatewayService.create(request);
    }

    @PutMapping("/{id}")
    public Gateway update(@PathVariable("id") String id, @RequestBody GatewayDTO request) {
        return this.gatewayService.update(id, request);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") String id) {
        this.gatewayService.delete(id);
    }

    @PatchMapping("/{id}/add")
    public Gateway addDevice(@PathVariable("id") String id, @RequestBody IdDTO<Long> deviceId) {
        return this.gatewayService.addDevice(id, deviceId.getId());
    }

    @PatchMapping("/{id}/remove")
    public Gateway removeDevice(@PathVariable("id") String id, @RequestBody IdDTO<Long> deviceId) {
        return this.gatewayService.removeDevice(id, deviceId.getId());
    }
}
