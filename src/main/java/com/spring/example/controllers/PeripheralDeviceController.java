package com.spring.example.controllers;

import com.spring.example.models.PeripheralDevice;
import com.spring.example.models.repositories.GatewayRepository;
import com.spring.example.models.repositories.PeripheralDeviceRepository;
import com.spring.example.schemas.PeripheralDeviceDTO;
import com.spring.example.services.PeripheralDeviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/device")
public class PeripheralDeviceController {
    private final PeripheralDeviceService peripheralDeviceService;

    @Autowired
    public PeripheralDeviceController(PeripheralDeviceService peripheralDeviceService) {
        this.peripheralDeviceService = peripheralDeviceService;
    }

    @GetMapping("")
    public List<PeripheralDeviceDTO> getAll() {
        return this.peripheralDeviceService.getAll();
    }

    @GetMapping("/{id}")
    public PeripheralDeviceDTO getOne(@PathVariable("id") Long id) {
        return this.peripheralDeviceService.getOne(id);
    }

    @PostMapping("")
    public PeripheralDevice create(@RequestBody PeripheralDeviceDTO request) {
        return this.peripheralDeviceService.create(request);
    }

    @PutMapping("/{id}")
    public PeripheralDevice update(@PathVariable("id") Long id, @RequestBody PeripheralDeviceDTO request) {
        return this.peripheralDeviceService.update(id, request);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Long id) {
        this.peripheralDeviceService.delete(id);
    }
}
